function strType(value,type){
  if(type==="STR"){
    return String(value);
  }
  if(typeof(value) === typeof("string")){
    if(value.indexOf(",") != -1){
      value = value.replace(",",".")
    }
  }

  if(isNaN(value)){
    return 0;
  }
  if(type==="INT"){
    try{
      value = parseInt(value);
    }catch{
      return 0;
    }
  }
  else if(type==="FLOAT"){
    try{
      value = parseFloat(value);
    }catch{
      return parseFloat(0);
    }
  }
  return value
}

function weightedAverage(values, weights){
  let ret_val = 0;
  for(let i = 0; i < values.length; i++){
    let value = values[i]
    let weight = weights[i]
    ret_val += value*weight;
  }
  return ret_val/sumArray(weights)
}

const sumArray = (arr) => arr.reduce((x, y) => x + y, 0);
const sumStrArray = (arr) => arr.reduce((x, y) => `${x}, ` + y);
const avgArray = (arr) => sumArray(arr) / arr.length
