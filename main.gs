
var _ = LodashGS.load();

function loadColumns(sheet, header=1, max_rows=100, max_cols=32)
{
  values = sheet.getRange(header,1,max_rows,max_cols).getValues();
  return values;
}

function reduceField(data, field, method, weights = {}){
  let keys = Object.keys(data)
  for(let i = 0; i < keys.length; i++)
  {
    let key = keys[i]
    let data_in = data[key][field]
    if(method === "SUM"){
      if(typeof(data_in[0]) !== typeof(1)){
        data[key][field] = sumStrArray(data_in)
      }else{
        data[key][field] = sumArray(data_in)
      }
    }else if(method === "AVG"){
      data[key][field] = avgArray(data_in)
    }else if(method === "WAVG"){
      if(weights[key] !== undefined){
        let ws = weights[key][Object.keys(weights[key])[0]]
        data[key][field] = weightedAverage(data_in, ws)
      }else{
        data[key][field] = 0;
      }

    }
  }

}

function mainFunction(read_sheet)
{
  const rsheet = SpreadsheetApp.getActive().getSheetByName(read_sheet);
  const unique_key = "some-key"
  let vals = loadColumns(rsheet);
  let header = vals.shift();
  let table = new Table(header, vals, unique_key);
}
