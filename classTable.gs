class Table
{
constructor(header, rows, unique)
{
  this.data = {};

  /*
  builds an object using unique as the key
  includes the unique in the object aswell along with its corresponding key
  */
  let unique_pos = header.indexOf(unique);
  for(let i = 0; i < rows.length; i++){
    let row = rows[i];
    for(let j = 0; j < row.length; j++){
      let value = row[j];
      let key = String(header[j]);
      if(key !== "" && row[unique_pos] !== "")
      {
        if(this.data[row[unique_pos]]){
          if(key in this.data[row[unique_pos]]){
            console.error(`Warning key used was not unique! -- unique key :${row[unique_pos]} key: ${key}`);
            break;
          }else{
            this.data[row[unique_pos]][key] = value;
          }

        }else{
          this.data[row[unique_pos]] = {};
          this.data[row[unique_pos]][key] = value;
        }
      }
    }
  }
}


combineFields(selector, fields)
{
  //combines fields into array with matching selector
  let r_data = {};
  let ids = Object.keys(this.data);

  for(let i = 0; i < ids.length; i++){
    let id = ids[i];
    let entry = this.data[id];
    if(r_data[entry[selector]]){
      for(let j = 0; j < fields.length; j++){
        let field = fields[j];
        let value = strType(entry[field["lable"]], field['type']);
        r_data[entry[selector]][field["lable"]].push(value);
      }
    }else{
      r_data[entry[selector]] = {};
      for(let j = 0; j < fields.length; j++){
        let field = fields[j];
        let value = strType(entry[field["lable"]], field['type']);
        r_data[entry[selector]][field["lable"]] = [value];
      }
    }
  }
  return r_data;
}

}
