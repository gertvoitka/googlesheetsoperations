function testFunction(func, args, expected, silent=true, success="", fail=""){
  val = func(...args)
  silent ? "" : console.log(`Running function ${func.name} with arguments: ${args}`)
  if(val === expected){
    silent ? "" : console.log(`Test passed for ${func.name}, ${success}`)
    return true;
  }else{
    console.error(`Test failed for ${func.name}, ${fail}\nexpected ${expected} got ${val} instead`)
    return false;
  }
}

function testStrType(){
  let pass = true;
  testFunction(strType, ['####', 'FLOAT'], 0) ? "" : pass=false;
  testFunction(strType, ['####', "INT"], 0) ? "" : pass=false;
  testFunction(strType, ['123,2', "FLOAT"], 123.2) ? "" : pass=false;
  testFunction(strType, ['321.2', "FLOAT"], 321.2) ? "" : pass=false;
  testFunction(strType, ['31', "INT"], 31) ? "" : pass=false;
  testFunction(strType, ['31.2', "INT"], 31) ? "" : pass=false;
  testFunction(strType, ['abc', "STR"], "abc") ? "" : pass=false;
  testFunction(strType, ['', "STR"], "") ? "" : pass=false;
  testFunction(strType, ['ab,c', "STR"], "ab,c") ? pass : pass=false;
  testFunction(strType, [32, "STR"], "32") ? "" : pass=false;
  return pass
}

function testAvgArray(){
  let pass = true;
  testFunction(avgArray,[[4,4,4]], 4) ? "" : pass=false;
  testFunction(avgArray,[[3,5,9]], 17/3) ? "" : pass=false;
  testFunction(avgArray,[[4,2,8,2]], 4) ? "" : pass=false;
  testFunction(avgArray,[[-4,-2,-8,-2]], -4) ? "" : pass=false;
  testFunction(avgArray,[[0,0,0,0]], 0) ? "" : pass=false;
  return pass
}

function testweightedAverage(){
  let pass = true;
  testFunction(weightedAverage,[[5,6,8],[0.2,0.3,0.5]],6.8) ? "" : pass=false;
  testFunction(weightedAverage,[[118.67, 57.13],[160.09, 7.8]], 115.81091369348978) ? "" : pass=false;
  return pass
}

function testTable(){
  let theader = ["a","b","c"];
  let pass = true;
  let rows = [["33","2","3"],["11","2","3"],["22","2","3"]];
  let unique = "a";
  let answer_data = {
    "33":{"a":"33","b":"2","c":"3"},
    "11":{"a":"11","b":"2","c":"3"},
    "22":{"a":"22","b":"2","c":"3"}
  }
  let answer_combine = { '2': { c: [ 3, 3, 3 ] } }
  let test_table = new Table(theader,rows,unique);
  if(_.isEqual(test_table.data, answer_data)){

  }else{
    console.error("Failed table construction test")
    pass = false;
  }
  if(_.isEqual(test_table.combineFields('b',[{lable:'c',type:"INT"}]), answer_combine)){

  }else{
    console.error("Failed combine table test")
    pass = false;
  }
  return pass;
}

function testSumArray(){
  let pass = true
  testFunction(sumArray, [[1,3,5]], 9) ? "" : pass=false;
  testFunction(sumArray, [[-1,3,5]], 7) ? "" : pass=false;
  testFunction(sumArray, [[0,0,0]], 0) ? "" : pass=false;
  testFunction(sumArray, [[0,2.2,3.5]], 5.7) ? "" : pass=false;
  return pass
}
function testStrSumArray(){
  pass = true
  testFunction(sumStrArray, [['32', '32-D']], "32, 32-D") ? "" : pass=false;
  testFunction(sumStrArray, [[1, 5]], "1, 5") ? "" : pass=false;
  testFunction(sumStrArray, [["a", "b", "c"]], "a, b, c") ? "" : pass=false;
  testFunction(sumStrArray, [["18", "b", "c"]], "18, b, c") ? "" : pass=false;
  return pass
}

function testReduceField(){
  let pass = true
  let test_data = {
    "a":{
      "b":[1,2,3],
      "c":["a","b","c"],
      "wavg":[5,6,8]
    }
  }
  let wavg_test_weights = {
    "a":{
      "ws":[0.2,0.3,0.5]
    }
  }
  let answer_data = {
    "a":{
      "b":6,
      "c":"a, b, c",
      "wavg":6.8
    }
  }
  reduceField(test_data,'b',"SUM");
  reduceField(test_data,'c',"SUM");
  reduceField(test_data,'wavg',"WAVG",wavg_test_weights);
  if(_.isEqual(test_data, answer_data)){

  }else{
    pass = false;
    console.error("Failed reduce field test..");
  }
  return pass
}

function runTests(){
  passed = true
  testTable() ? console.log("passed table tests") : passed = false;
  testStrType() ? console.log("passed strType tests") : passed = false;
  testSumArray() ? console.log("passed sumArray tests") : passed = false;
  testStrSumArray() ? console.log("passed sumStrArray tests") : passed = false;
  testAvgArray() ? console.log("passed avgArray tests") : passed = false;
  testReduceField() ? console.log("passed reduce field tests") : passed = false;
  testweightedAverage() ? console.log("passed weigthed average tests") : passed = false;
  return passed
}
