# Convert Google Sheet Table into Javascript Object

This simple script converts a table with specified header row into a javascript Object
on which operations can be run on. One field should contain unique key to avoid over writing


# Examples

```javascript
let read_sheet = "some-sheet-name";
const rsheet = SpreadsheetApp.getActive().getSheetByName(read_sheet);
const unique_key = "some-key";
let vals = loadColumns(rsheet);
let header = vals.shift();
let table = new Table(header, vals, unique_key);
```

this creates a table object, where the some-key header is used as
the unique key

```javascript
let group_by = "some-field";
let data = table.combineFields(group_by, [{field:"field-1", type:"INT"}, {field:"field-2", type:"INT"}]);
```

this will combine any matching fields and add their fields to list aswell process their type

```javascript
reduceField(data, "field-1", "AVG");
```
this reduces the fields list by method determined
options as of now are:


* AVG
* SUM (for strings and ints)
* WAVG (requires weights parameter)


# Depends on

LodashGS, for testing the code. can be ignored if not using the tests.gs
